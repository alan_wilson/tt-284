<!DOCTYPE html>
<html lng="en">
<head>
<title>Database Contents</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<style>
    div {color: red; padding: 10px; border-style: solid; border-color: cornflowerblue;}
</style>
</head>

<body>
<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);
// Include our database connection functions   
include 'mydatabase.php';
include 'helpers.inc.php';
    
//$mydatabase = 'test';

$dbhandle = mysqli_connect($hostname, $username, $password) or die( "Unable to connect to MySQL");

$selected = mysqli_select_db($dbhandle, $mydatabase) or die("Unable to connect to " . $mydatabase );

$sql = "SHOW TABLES";

$result = mysqli_query($dbhandle, $sql) or die ("Could not execute the query " . $sql );   

while ($row = mysqli_fetch_array($result)) {

    //var_dump ($row);

	echo "<div>";
	htmlout($row[0]);
	echo "</div>";

}

?>
</body>
</html>
