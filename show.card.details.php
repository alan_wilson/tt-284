<!DOCTYPE html>
<html lng="en">
<head>
<title>Card Details</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="../ba_example/public/css/app.css" type="text/css" rel="stylesheet">
    
    <meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
</head>
<body>
<?php
    error_reporting(E_ALL);
    ini_set('display_errors', 1);
    define('ISITSAFETORUN', TRUE); 

    // Include our database connection functions   
    include 'mydatabase.php';
    include 'helpers.inc.php';
    
//$mydatabase = 'test';

    $dbhandle = mysqli_connect($hostname, $username, $password) 
        or die( "Unable to connect to MySQL");
    $selected = mysqli_select_db($dbhandle, $mydatabase) or die("Unable to connect to " . $mydatabase );
    $sql = "SELECT * FROM credit_card_details ORDER BY name";
    $result = mysqli_query($dbhandle,$sql) or die ("Could not execute the query " . $sql );  
    
    echo 
    '<main class="py-4">
         
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="panel">Card index</div>
                        <div class="flex flex-wrap mb-4 card-body">';

                    while ($row = mysqli_fetch_array($result)) 
                    {;
                    ?>  
    
                     
                        
                            <div class="m-2 w-1/3 max-w-sm border border-blue-800 rounded overflow-hidden shadow-lg">
                                <div class="px-6 py-4">
                                    <p class="text-gray-700 text-base">
                                          <?php htmlout($row['number']); ?>
                                        </p>
                                </div> 
                                <div class="px-6 py-4">
                                    <span class="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2"><?php htmlout($row['start_date']); ?></span> 
                                    <span class="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2"><?php htmlout($row['end_date']); ?></span>
                                </div> 
                                <div class="font-bold text-xl mb-2 ml-4"><?php htmlout($row['name']); ?></div> 
                                <div class="font-semibold text-sm ml-4">99-12-88</div>    
                            </div>

                    
                    <?php } echo '
                        </div>
                   </div>
                </div>
            </div>
        </div>
    </div>
    </main>';

?>

</body>
</html>
