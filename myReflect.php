<?php

// echo start of the HTML page (this is not done well as the focus here is on PHP)
echo "<html><body>";

echo "<p>For this to work, the parameter to $_POST must match the field name on the form</p>";

echo "<p>(Normally we would not access these super global variables directly)</p>";

echo "<p>Client Name = ". $_POST['client'] . "</p>";
echo "<p> Date = ". $_POST['date'] . "</p>";
echo "<p> Amount = ". $_POST['amount'] . "</p>";

// echo end of HTML page.
echo"</body></html>";