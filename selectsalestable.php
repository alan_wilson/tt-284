<!DOCTYPE html>
<html lang="en">
<head>
<title>Results Table</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<body>

<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);
// Define a constant allowing this script to run
define('ISITSAFETORUN', TRUE); 

include 'mydatabase.php';

$dbhandle = mysqli_connect($hostname, $username, $password) or die( "Unable to connect to MySQL");
    
$selected = mysqli_select_db($dbhandle, $mydatabase) or die("Unable to connect to " . $mydatabase );

$result = mysqli_query($dbhandle, "SELECT * FROM sales");

echo "<table border=\"1\" bordercolor=\"#FFCC00\" style=\"background-color:#FFFFCC\" width=\"50%\" cellpadding=\"3\" cellspacing=\"3\"><tr>";

while ($row = mysqli_fetch_array($result)) {

  echo "<tr>";
  echo "<td>".$row['client'] . "</td><td>" . $row['date']. "</td><td>" . $row['amount']."</td>";
  echo "</tr>";

}

echo "</tr></table>";

?>

</body>
</html>
